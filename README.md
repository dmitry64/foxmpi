# Fox algorithm implementation with MPI in C++

## Build instructions

Make sure you have MPI, C++ compiler and cmake installed before building.

Build instructions:
```sh
git clone https://gitlab.com/dmitry64/foxmpi.git
cd foxmpi
mkdir build
cd build
cmake ..
make
```

Run with `mpirun`:
```sh
mpirun -n 4 ./foxmpi
```

Example result:
```
Grid setup finished, total number of processors: 4
Grid dimensions: 2x2
Generating random matrices...
Generating 1024x1024 matrix (~4 MB)
Generating 1024x1024 matrix (~4 MB)
Matrices generated successfully!
Sending generated matrices to other processes
Matrices syncronized successfully
Preparing local per-process buffers...
Ready for multiplication
Starting multiplication with Fox algorithm...
Fox algorithm multiplication finished!
Fox time: 0.67292s
Combining multiplication results...
Verifying multiplication result...
Simple multiplication time: 3.09674s
Matrices multiplied successfully!
```