#include "mpi.h"
#include <cstddef>
#include <ctime>
#include <math.h>
#include <stdlib.h>

#define MATRIX_SIZE 1024

typedef int *PackedMatrix;    // Matrix contents are stored in a single array
typedef int **UnpackedMatrix; // Matrix contents are stored in array of arrays

typedef struct
{
    int proc_count;
    int dim;
    int row;
    int col;
    int grid_rank;
    int world_rank;
    MPI_Comm grid_comm;
    MPI_Comm row_comm;
    MPI_Comm col_comm;
} GridStructure;

bool is_even_power_of_two(int n)
{
    int x = 0;
    while (x < n)
    {
        int value = pow(2, x);
        if (value == n)
        {
            return x % 2 == 0;
        }
        x++;
    }
    return false;
}

void create_mpi_grid_topology(GridStructure *grid)
{
    int dimensions[2];
    int wrap_around[2];
    int coordinates[2];
    int free_coords[2];

    MPI_Barrier(MPI_COMM_WORLD); // Wait for all processes to come to this point

    MPI_Comm_size(MPI_COMM_WORLD, &(grid->proc_count)); // Save total process count
    MPI_Comm_rank(MPI_COMM_WORLD, &(grid->world_rank)); // Save rank of current process

    if (!is_even_power_of_two(grid->proc_count))
    {
        if (grid->world_rank == 0)
        {
            std::cerr << "Error! Number of processors should be even power of two (1, 4, 16, 64, etc...)" << std::endl;
        }
        MPI_Finalize();
        exit(1);
    }

    MPI_Barrier(MPI_COMM_WORLD); // Wait for all processes to come to this point

    grid->dim = (int)sqrt((double)grid->proc_count);
    dimensions[0] = grid->dim;
    dimensions[1] = grid->dim;

    wrap_around[0] = 0;
    wrap_around[1] = 1;

    MPI_Barrier(MPI_COMM_WORLD); // Wait for all processes to come to this point

    MPI_Cart_create(MPI_COMM_WORLD, 2, dimensions, wrap_around, 1, &(grid->grid_comm));
    MPI_Comm_rank(grid->grid_comm, &(grid->grid_rank));
    MPI_Cart_coords(grid->grid_comm, grid->grid_rank, 2, coordinates);
    grid->row = coordinates[0];
    grid->col = coordinates[1];

    free_coords[0] = 0;
    free_coords[1] = 1;
    MPI_Cart_sub(grid->grid_comm, free_coords, &(grid->row_comm));

    free_coords[0] = 1;
    free_coords[1] = 0;
    MPI_Cart_sub(grid->grid_comm, free_coords, &(grid->col_comm));

    MPI_Barrier(grid->grid_comm); // Wait for all processes to come to this point
}

void multiply_local(UnpackedMatrix a, UnpackedMatrix b, UnpackedMatrix c, int size)
{
    int temp = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            temp = 0;
            for (int k = 0; k < size; k++)
            {
                temp += (a[i][k] * b[k][j]);
            }
            c[i][j] += temp;
        }
    }
}

void unpack_matrix(PackedMatrix buff, UnpackedMatrix a, int size)
{
    int k = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            a[i][j] = buff[k];
            k++;
        }
    }
}

void pack_matrix(PackedMatrix buff, UnpackedMatrix a, int size)
{
    int k = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            buff[k] = a[i][j];
            k++;
        }
    }
}

void print_matrix(UnpackedMatrix matrix, int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << "|";
        for (int j = 0; j < size; j++)
        {
            int el = matrix[i][j];
            if (el < 10)
                std::cout << " ";
            std::cout << el;
            std::cout << "|";
        }
        std::cout << std::endl;
    }
}

void print_packed_matrix(PackedMatrix matrix, int size)
{
    for (int i = 0; i < size; i++)
    {
        std::cout << "|";
        for (int j = 0; j < size; j++)
        {
            int el = matrix[i * size + j];
            if (el < 10)
                std::cout << " ";
            std::cout << el;
            std::cout << "|";
        }
        std::cout << std::endl;
    }
}

PackedMatrix generate_random_matrix()
{
    std::cout << "Generating " << MATRIX_SIZE << "x" << MATRIX_SIZE << " matrix (~"
              << (MATRIX_SIZE * MATRIX_SIZE * 4) / (1024 * 1024) << " MB)" << std::endl;
    PackedMatrix matrix = new int[MATRIX_SIZE * MATRIX_SIZE];

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        int *row = &matrix[i * MATRIX_SIZE];
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            row[j] = rand() % 100;
        }
    }

    return matrix;
}

void fox_multiply(int n, GridStructure *grid, UnpackedMatrix a, UnpackedMatrix b, UnpackedMatrix c)
{
    MPI_Status status;

    int block_size = n / grid->dim;

    UnpackedMatrix temp_a = new int *[block_size];
    for (int i = 0; i < block_size; ++i)
        temp_a[i] = new int[block_size];
    for (int i = 0; i < block_size; i++)
        for (int j = 0; j < block_size; j++)
            temp_a[i][j] = 0;

    PackedMatrix buff = new int[block_size * block_size];
    for (int i = 0; i < block_size * block_size; i++)
        buff[i] = 0;

    int src = (grid->row + 1) % grid->dim;
    int dst = (grid->row + grid->dim - 1) % grid->dim;

    for (int stage = 0; stage < grid->dim; stage++)
    {
        int root = (grid->row + stage) % grid->dim;
        if (root == grid->col)
        {
            pack_matrix(buff, a, block_size);
            MPI_Bcast(buff, block_size * block_size, MPI_INT, root, grid->row_comm);
            unpack_matrix(buff, a, block_size);
            multiply_local(a, b, c, block_size);
        }
        else
        {
            pack_matrix(buff, temp_a, block_size);
            MPI_Bcast(buff, block_size * block_size, MPI_INT, root, grid->row_comm);
            unpack_matrix(buff, temp_a, block_size);
            multiply_local(temp_a, b, c, block_size);
        }
        pack_matrix(buff, b, block_size);
        MPI_Sendrecv_replace(buff, block_size * block_size, MPI_INT, dst, 0, src, 0, grid->col_comm, &status);
        unpack_matrix(buff, b, block_size);
    }
}

UnpackedMatrix test_single_thread(PackedMatrix a, PackedMatrix b)
{
    UnpackedMatrix test_a = new int *[MATRIX_SIZE];
    UnpackedMatrix test_b = new int *[MATRIX_SIZE];
    UnpackedMatrix test_result = new int *[MATRIX_SIZE];

    for (int i = 0; i < MATRIX_SIZE; ++i)
    {
        test_a[i] = new int[MATRIX_SIZE];
        test_b[i] = new int[MATRIX_SIZE];
        test_result[i] = new int[MATRIX_SIZE];
    }

    for (int i = 0; i < MATRIX_SIZE; i++)
    {
        for (int j = 0; j < MATRIX_SIZE; j++)
        {
            test_a[i][j] = a[i * MATRIX_SIZE + j];
            test_b[i][j] = b[i * MATRIX_SIZE + j];
            test_result[i][j] = 0;
        }
    }

    multiply_local(test_a, test_b, test_result, MATRIX_SIZE);

    // std::cout << "Local result:" << std::endl;
    // PrintMatrix(test_result, MATRIX_SIZE);
    return test_result;
}


int main(int argc, char *argv[])
{
    int block_size;
    clock_t start, finish;
    MPI_Init(&argc, &argv);

    MPI_Barrier(MPI_COMM_WORLD); // Wait for all processes to come to this point

    GridStructure grid;
    create_mpi_grid_topology(&grid);

    MPI_Barrier(grid.grid_comm); // Wait for all processes to come to this point

    if (grid.world_rank == 0)
    {
        std::cout << "===================================================" << std::endl;
        std::cout << "Grid setup finished, total number of processors: " << grid.proc_count << std::endl;
        std::cout << "Grid dimensions: " << grid.dim << "x" << grid.dim << std::endl;
    }

    MPI_Barrier(grid.grid_comm); // Wait for all processes to come to this point

    PackedMatrix a = new int[MATRIX_SIZE * MATRIX_SIZE];
    PackedMatrix b = new int[MATRIX_SIZE * MATRIX_SIZE];

    if (grid.world_rank == 0)
    {
        std::cout << "Generating random matrices..." << std::endl;
        a = generate_random_matrix();
        b = generate_random_matrix();
        std::cout << "Matrices generated successfully!" << std::endl;
    }

    MPI_Barrier(grid.grid_comm);

    if (grid.world_rank == 0)
    {
        std::cout << "Sending generated matrices to other processes" << std::endl;
    }

    MPI_Bcast(a, MATRIX_SIZE * MATRIX_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Barrier(grid.grid_comm);
    MPI_Bcast(b, MATRIX_SIZE * MATRIX_SIZE, MPI_INT, 0, MPI_COMM_WORLD);

    if (grid.world_rank == 0)
    {
        std::cout << "Matrices syncronized successfully" << std::endl;
    }

    MPI_Barrier(grid.grid_comm);

    if (grid.world_rank == 0)
    {
        std::cout << "Preparing local per-process buffers..." << std::endl;
    }

    block_size = MATRIX_SIZE / grid.dim;
    int base_row = grid.row * block_size;
    int base_col = grid.col * block_size;

    UnpackedMatrix local_a = new int *[block_size];
    UnpackedMatrix local_b = new int *[block_size];
    UnpackedMatrix local_c = new int *[block_size];

    for (int i = 0; i < block_size; ++i)
    {
        local_a[i] = new int[block_size];
        local_b[i] = new int[block_size];
        local_c[i] = new int[block_size];
    }

    for (int i = base_row; i < base_row + block_size; i++)
    {
        for (int j = base_col; j < base_col + block_size; j++)
        {
            local_a[i - (base_row)][j - (base_col)] = a[i * MATRIX_SIZE + j];
            local_b[i - (base_row)][j - (base_col)] = b[i * MATRIX_SIZE + j];
            local_c[i - (base_row)][j - (base_col)] = 0;
        }
    }

    if (grid.world_rank == 0)
    {
        std::cout << "Ready for multiplication" << std::endl;
    }

    MPI_Barrier(grid.grid_comm);
    if (grid.world_rank == 0)
    {
        std::cout << "Starting multiplication with Fox algorithm..." << std::endl;
        start = clock();
    }

    fox_multiply(MATRIX_SIZE, &grid, local_a, local_b, local_c);

    MPI_Barrier(grid.grid_comm);
    if (grid.world_rank == 0)
    {
        finish = clock();
        std::cout << "Fox algorithm multiplication finished!" << std::endl;
        std::cout << "Fox time: " << double(finish - start) / CLOCKS_PER_SEC << "s" << std::endl;
    }

    PackedMatrix result_buff = new int[MATRIX_SIZE * MATRIX_SIZE];
    PackedMatrix local_buff = new int[block_size * block_size];

    pack_matrix(local_buff, local_c, block_size);

    MPI_Gather(local_buff, block_size * block_size, MPI_INT, result_buff, block_size * block_size, MPI_INT, 0,
               grid.grid_comm);
    MPI_Barrier(grid.grid_comm);

    if (grid.world_rank == 0)
    {
        std::cout << "Combining multiplication results..." << std::endl;
        PackedMatrix data = new int[MATRIX_SIZE * MATRIX_SIZE];
        int k = 0;
        for (int bi = 0; bi < grid.dim; bi++)
            for (int bj = 0; bj < grid.dim; bj++)
                for (int i = bi * block_size; i < bi * block_size + block_size; i++)
                    for (int j = bj * block_size; j < bj * block_size + block_size; j++)
                    {
                        data[i * MATRIX_SIZE + j] = result_buff[k];
                        k++;
                    }

        std::cout << "Verifying multiplication result..." << std::endl;

        // PrintPackedMatrix(data, MATRIX_SIZE);

        start = clock();
        UnpackedMatrix res = test_single_thread(a, b);
        finish = clock();
        std::cout << "Simple multiplication time: " << double(finish - start) / CLOCKS_PER_SEC << "s" << std::endl;

        for (int i = 0; i < MATRIX_SIZE; i++)
        {
            for (int j = 0; j < MATRIX_SIZE; j++)
            {
                if (res[i][j] != data[i * MATRIX_SIZE + j])
                {
                    std::cerr << "Error! Multiplication results are not matching" << std::endl;
                    exit(1);
                }
            }
        }
        std::cout << "Matrices multiplied successfully!" << std::endl;
    }

    MPI_Finalize();
    exit(0);
}
